import { combineReducers } from 'redux';

import users from './reducer_users';
import filteredData from './reducer_filter';
import sortData from './reducer_sort';

export default combineReducers({
    usersData: users,
    filteredData: filteredData,
    sortedData: sortData
});
