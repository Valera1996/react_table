import Immutable from 'immutable';

export default function sortData (state = '', action) {
    if (action.type === 'SORT_DATA') {

        let data=action.users;
        let fiends = Immutable.fromJS(data);

        if(action.sortType){
            var sorted = fiends.sort(
                (a, b) => a.get(action.field).localeCompare(b.get(action.field))
            );
        }
            else{
            var sorted = fiends.sort(
                (a, b) => b.get(action.field).localeCompare(a.get(action.field))
            );
        }
        let sortedList = sorted.toJS();
            return sortedList;
    }

    return state;
}