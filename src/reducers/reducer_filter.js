export default function filteredData (state = '', action) {
    if (action.type === 'FIND_DATA') {
       let filteredData=action.data.filter(data =>
            data.name.toLowerCase().includes(action.findField.toLowerCase()) ||
            data.city.toLowerCase().includes(action.findField.toLowerCase()) ||
            data.state.toLowerCase().includes(action.findField.toLowerCase()) ||
            data.country.toLowerCase().includes(action.findField.toLowerCase()) ||
            data.company.toLowerCase().includes(action.findField.toLowerCase())  )
        return filteredData;
    }
    return state;
}