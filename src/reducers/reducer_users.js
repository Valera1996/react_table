const initState = [
    {
        "_id": "1",
        "name": "Valery Goncharov",
        "city": "Caln",
        "state": "American Samoa",
        "country": "Mauritius",
        "company": "Rocklogic",
        "favouriteNumber": 27
    }
];

export default function users (state = initState, action)  {
    if(action.type === 'ADD_DATA')
    {
        return action.data;
    }
    return state;
}