import React, { Component } from 'react';

class SearchBar extends Component {

    render () {
        let searchInput='';
        return ( <div >
            <h2 className="table-header">Bordered Table</h2>
            <div>
                <input
                    className="search-input"
                    type="text"
                    ref={(input) => { searchInput = input}} />
                <button className="search-button" onClick={()=>{ this.props.findTrack (searchInput.value); searchInput.value = ''   }}>Find data</button>
            </div>
        </div> );
    };
}

export default SearchBar;