import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import SearchBar from '../components/SearchBar';
import * as actions from '../actions';


class App extends Component {

    constructor (props) {
        super(props);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.findTrack = this.findTrack.bind(this);
        this.onSort = this.onSort.bind(this);
        this.state={upOrDown: true, isSorted: false};
    };

    componentDidMount () {
       fetch('../../json_data/info.json')
            .then( (response) => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then( (data) => {
                this.props.onAddData(data);
            });
    };

    findTrack (input) {
       fetch('../../json_data/info.json')
            .then( (response) => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then( (data)=>  {
                this.props.onFindData(input, data);
            });
    };


     onSort (field) {

        if(this.state.upOrDown)
        {
            this.setState({upOrDown:false});
        }

        else
        {
            this.setState({upOrDown:true});
        }

        this.setState({isSorted : true});

        this.props.onSortData(this.props.users, this.state.upOrDown, field);
    };

    render () {

        return (
            <div className="container">

                <SearchBar findTrack={this.findTrack}  />
                <button onClick={this.props.getAsyncAction}>Create async action</button>
                <table className="table table-bordered">
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>name <button onClick={() => this.onSort('name')}> <i className="fa fa-fw fa-sort" > </i></button> </th>
                        <th>city <button onClick={() => this.onSort('city')}> <i className="fa fa-fw fa-sort" > </i></button></th>
                        <th>state <button onClick={() => this.onSort('state')}> <i className="fa fa-fw fa-sort" > </i></button></th>
                        <th>country <button onClick={() => this.onSort('country')}> <i className="fa fa-fw fa-sort" > </i></button></th>
                        <th>company <button onClick={() => this.onSort('company')}> <i className="fa fa-fw fa-sort" > </i></button></th>
                        <th>favouriteNumber </th>
                    </tr>
                    </thead>

                    <tbody>
                    { this.state.isSorted ?
                    this.props.sortedUsers.map(data => (
                        <tr key={data._id}>
                            <td>{data._id}</td>
                            <td>{data.name}</td>
                            <td>{data.city}</td>
                            <td>{data.state}</td>
                            <td>{data.country}</td>
                            <td>{data.company}</td>
                            <td>{data.favouriteNumber}</td>
                        </tr>
                    ))
                    :
                    this.props.users.map(data => (
                        <tr key={data._id}>
                            <td>{data._id}</td>
                            <td>{data.name}</td>
                            <td>{data.city}</td>
                            <td>{data.state}</td>
                            <td>{data.country}</td>
                            <td>{data.company}</td>
                            <td>{data.favouriteNumber}</td>
                        </tr>
                    ))}
                    </tbody>

                </table>
            </div>
        );
    };
}

export default connect(
    state => ({
        users: state.filteredData ? state.filteredData: state.usersData,
        sortedUsers: state.filteredData ? state.filteredData: state.sortedData
    }),
    dispatch => bindActionCreators(actions, dispatch)

)(App);