export function onSortData (data, sortType, field) {
    const SORT_DATA='SORT_DATA';
    return {
        type: SORT_DATA,
        users: data,
        sortType: sortType,
        field:field
    }
}
export function onAddData (data) {
    const ADD_DATA='ADD_DATA';
    return {
        type: ADD_DATA,
        data: data
    }
};
export function onFindData (name, data) {
    const FIND_DATA='FIND_DATA';
    return {
        type: FIND_DATA,
        findField: name,
        data: data
    }
};
export const getAsyncAction = () => dispatch =>{
        setTimeout(() => {
            console.log('check');
            dispatch({
                type: 'ASYNC',
                payload: []
            })
        }, 2000);
};



