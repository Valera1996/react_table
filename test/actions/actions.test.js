import * as actions from '../../src/actions'
import * as types from '../../src/constants/ActionTypes'

describe('actions', () => {
    const data = [
        {
            "_id": "598827e6cb44473db5f78ce9",
            "name": "Natasha Alston",
            "city": "Ernstville",
            "state": "Ohio",
            "country": "Japan",
            "company": "Biotica",
            "favouriteNumber": 38
        },
        {
            "_id": "598827e6e3e05c46c69a348b",
            "name": "Massey Gilliam",
            "city": "Caln",
            "state": "American Samoa",
            "country": "Mauritius",
            "company": "Rocklogic",
            "favouriteNumber": 35
        }];

    it('should create an action to add/initialize data', () => {
        const expectedAction = {
            type: types.ADD_DATA,
            data: data
        };
        expect(actions.onAddData(data)).toEqual(expectedAction);
    });

    it('should create a new sorted data', () => {
        const sortType = true ;
        const field =  'name';
        const expectedAction = {
            type: types.SORT_DATA,
            users: data,
            sortType: sortType,
            field:field
        };
        expect(actions.onSortData(data, sortType,field)).toEqual(expectedAction);
    });

    it('should create a new data filtered by search field', () => {
       const name = 'Natasha';
        const expectedAction = {
            type: types.FIND_DATA,
            findField: name,
            data: data
        };
        expect(actions.onFindData(name, data)).toEqual(expectedAction);
    });

});
