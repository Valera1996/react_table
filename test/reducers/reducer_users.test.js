import users from '../../src/reducers/reducer_users';
import * as types from '../../src/constants/ActionTypes'

const initState = [
    {
        "_id": "1",
        "name": "Valery Goncharov",
        "city": "Caln",
        "state": "American Samoa",
        "country": "Mauritius",
        "company": "Rocklogic",
        "favouriteNumber": 27
    }
];

const data = [
    {
        "_id": "598827e6cb44473db5f78ce9",
        "name": "Natasha Alston",
        "city": "Ernstville",
        "state": "Ohio",
        "country": "Japan",
        "company": "Biotica",
        "favouriteNumber": 38
    },
    {
        "_id": "598827e6e3e05c46c69a348b",
        "name": "Massey Gilliam",
        "city": "Caln",
        "state": "American Samoa",
        "country": "Mauritius",
        "company": "Rocklogic",
        "favouriteNumber": 35
    }];

describe('ADD_DATA reducer', () => {
    it('should return the initial state', () => {
        expect(users(undefined, {})).toEqual(initState)
    })

    it('should handle ADD_DATA', () => {
        expect(
            users([], {
                type: types.ADD_DATA,
                data: data
            })
        ).toEqual(data)
        expect(
            users(
                 initState ,
                {
                    type: 'WRONG ACTION',
                    data: 'DATA'
                }
            )
        ).toEqual(initState)
    })
})