import sortData from '../../src/reducers/reducer_sort';
import * as types from '../../src/constants/ActionTypes'

const initState = [
    {
        "_id": "1",
        "name": "Valery Goncharov",
        "city": "Caln",
        "state": "American Samoa",
        "country": "Mauritius",
        "company": "Rocklogic",
        "favouriteNumber": 27
    }
];

const data = [
    {
        "_id": "598827e6cb44473db5f78ce9",
        "name": "Natasha Alston",
        "city": "Ernstville",
        "state": "Ohio",
        "country": "Japan",
        "company": "Biotica",
        "favouriteNumber": 38
    },
    {
        "_id": "598827e6e3e05c46c69a348b",
        "name": "Massey Gilliam",
        "city": "Caln",
        "state": "American Samoa",
        "country": "Mauritius",
        "company": "Rocklogic",
        "favouriteNumber": 35
    }];

describe('sortData reducer', () => {

    it('should return the initial state', () => {
        expect(sortData(undefined, {})
        ).toEqual("")
    });

    it('should handle SORT_DATA', () => {
        expect(
            sortData([], {
                type: types.SORT_DATA,
                users: data,
                sortType:true,
                field: 'name'
            })
        ).toEqual(
            [{
                "_id": "598827e6e3e05c46c69a348b",
                "name": "Massey Gilliam",
                "city": "Caln",
                "state": "American Samoa",
                "country": "Mauritius",
                "company": "Rocklogic",
                "favouriteNumber": 35
            },
            {
                "_id": "598827e6cb44473db5f78ce9",
                "name": "Natasha Alston",
                "city": "Ernstville",
                "state": "Ohio",
                "country": "Japan",
                "company": "Biotica",
                "favouriteNumber": 38
            }])
    });

    it('shouldn`t handle wrong action', () => {
        expect(
            sortData( initState ,
                {
                    type: 'WRONG ACTION',
                    data: 'DATA'
                }
            )
        ).toEqual(initState)
    });

});