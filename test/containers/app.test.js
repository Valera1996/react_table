import React, { Component } from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux'
import { createStore} from 'redux';
import App from '../../src/containers/app';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from '../../src/reducers';
const store = createStore(reducer,  composeWithDevTools());
global.fetch = jest.fn().mockImplementation(() =>
    Promise.resolve({ok: true, CorrelationId: '123'}));
global.fetch = jest.fn().mockImplementation(() => Promise.resolve({ok: true, CorrelationId: '123'}));

function setup() {
    const enzymeWrapper = mount(<Provider store={store}><App /></Provider>);
    return enzymeWrapper;
}

describe('containers', () => {
    describe('App', () => {
        it('should render self', () => {
            const  enzymeWrapper  = setup();
            expect(enzymeWrapper.find('table').hasClass('table-bordered')).toBe(true);
            const todoInputProps = enzymeWrapper.find('SearchBar').props();
            console.log(todoInputProps)

        });
    });
});