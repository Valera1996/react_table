import React from 'react';
import { mount } from 'enzyme';
import SearchBar from '../../src/components/SearchBar';

function setup() {
    const props = {
        findTrack: jest.fn()
    };
    const enzymeWrapper = mount(<SearchBar {...props} />);

    return {
        props,
        enzymeWrapper
    };
}

describe('components', () => {
    describe('SearchBar', () => {
        it('should render self ', () => {
            const { enzymeWrapper } = setup();

            expect(enzymeWrapper.find('h2').hasClass('table-header')).toBe(true);
            expect(enzymeWrapper.find('h2').text()).toBe('Bordered Table');

            const tableInput = enzymeWrapper.find('input');
            expect(tableInput.hasClass('search-input')).toBe(true);

        });
    });
});